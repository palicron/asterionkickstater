﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCtr : DamageTaker
{



	public float force;
	public float rad;
	public Rigidbody[] prb;
	public Collider[] pcol;
	public Rigidbody rib;
	public GameObject particles;
	// Use this for initialization
	void Start()
	{
		particles.SetActive(false);
		rib = this.GetComponentInParent<Rigidbody>();
	

	}

	// Update is called once per frame
	void Update()
	{

	}


	public override bool takeDmg(float damg, Vector3 origin, float force)
	{
	
		activeAnfor(origin);
		rib.AddExplosionForce(force, origin, rad);

		

		return true;
	}

	private void activeAnfor(Vector3 orignis)
	{
		particles.SetActive(true);
		for (int i = 0; i < prb.Length; i++)
		{
			float ranx = Random.Range(-2, 2);
			float rany = Random.Range(-2, 2);
			prb[i].useGravity = true;
			prb[i].isKinematic = false;

			Vector3 v = prb[i].transform.position - orignis;
			v.x += ranx;
			v.y += rany;
			prb[i].AddForce(-v * (force+ rany), ForceMode.Impulse);
			pcol[i].enabled = false;


		}
	}

	private void destroid()
	{
		GameObject.Destroy(this.gameObject);
	}
}
