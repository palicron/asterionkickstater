﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anforactr : DamageTaker {
	
	public GameObject[] parts;
	public float force;
	public float rad;
	public Rigidbody[] prb;
	public Collider[] pcol;
	public Rigidbody rib;
	public Collider parentcol;
	// Use this for initialization
	void Start () {

		rib = this.GetComponentInParent<Rigidbody>();
		parentcol = this.GetComponentInParent<Collider>();
		//for (int i=0;i>parts.Length;i++)
		//{
		//	prb[i] = parts[i].GetComponent<Rigidbody>();
		//	prb[i].useGravity = false;
		//	prb[i].isKinematic = true;
		//	pcol[i] = parts[i].GetComponent<Collider>();
		//	pcol[i].enabled = false;
		//}
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public override bool takeDmg(float damg, Vector3 origin, float force)
	{
		this.rib.isKinematic = true;
		this.rib.useGravity = false;
		activeAnfor();
		rib.AddExplosionForce(force, transform.position, rad);
	
	  parentcol.enabled = false;
		
		return true;
	}

	private void activeAnfor()
	{
		for (int i = 0; i < parts.Length; i++)
		{
			
			prb[i].useGravity = true;
			prb[i].isKinematic = false;
		
			pcol[i].enabled = true;
		}
	}

	private void destroid()
	{
		GameObject.Destroy(this.gameObject);
	}
}
