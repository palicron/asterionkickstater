// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Invibleshader"
{
	Properties
	{
		[HDR]_Color0("Color 0", Color) = (0.490566,0,0,0)
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Blend("Blend", Float) = 1
		_Color("Color", Float) = 1
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		_Teleport("Teleport", Range( -20 , 20)) = -5.832579
		_Range("Range", Range( -10 , 10)) = 0
		_Float0("Float 0", Float) = 1
		_RangeofOP("RangeofOP", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		GrabPass{ }
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPos;
			float3 worldPos;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _Color0;
		uniform sampler2D _GrabTexture;
		uniform float _Color;
		uniform float _Blend;
		uniform float _Teleport;
		uniform float _Range;
		uniform float _Float0;
		uniform float _RangeofOP;
		uniform float _Cutoff = 0.5;


		inline float4 ASE_ComputeGrabScreenPos( float4 pos )
		{
			#if UNITY_UV_STARTS_AT_TOP
			float scale = -1.0;
			#else
			float scale = 1.0;
			#endif
			float4 o = pos;
			o.y = pos.w * 0.5f;
			o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
			return o;
		}


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			o.Normal = UnpackNormal( tex2D( _TextureSample0, uv_TextureSample0 ) );
			o.Albedo = _Color0.rgb;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( ase_screenPos );
			float4 screenColor2 = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD( ase_grabScreenPos ) );
			float4 temp_cast_1 = (_Color).xxxx;
			float4 lerpResult4 = lerp( screenColor2 , temp_cast_1 , _Blend);
			o.Emission = lerpResult4.rgb;
			o.Alpha = 1;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform10 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float Gradient15 = ( ( transform10.y + _Teleport ) / _Range );
			float2 panner25 = ( _Time.y * float2( 0,0 ) + float2( 0,0 ));
			float2 uv_TexCoord27 = i.uv_texcoord * float2( 5,5 ) + panner25;
			float simplePerlin2D28 = snoise( uv_TexCoord27 );
			float Noise31 = ( simplePerlin2D28 + _Float0 );
			float Opa40 = ( ( ( 1.0 - Gradient15 ) * Noise31 ) - ( Gradient15 * _RangeofOP ) );
			clip( Opa40 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
-1272;211;1266;994;-316.3701;-40.87953;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;23;-637.4938,1245.844;Float;False;Constant;_Speed;Speed;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;9;-796.7787,616.3571;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;24;-476.2617,1260.515;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;10;-568.8799,562.6085;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-473.7256,828.5249;Float;False;Property;_Teleport;Teleport;5;0;Create;True;0;0;False;0;-5.832579;7.1;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;26;-363.2617,1060.515;Float;False;Constant;_Vector0;Vector 0;5;0;Create;True;0;0;False;0;5,5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PannerNode;25;-242.2617,1241.515;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;11;-210.5444,676.4222;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-126.2617,1052.515;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;14;-38.07158,873.4044;Float;False;Property;_Range;Range;6;0;Create;True;0;0;False;0;0;-1.09;-10;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;28;145.4796,1097.096;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;13;23.31676,733.0882;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;158.3182,1351.214;Float;False;Property;_Float0;Float 0;7;0;Create;True;0;0;False;0;1;1.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;15;278.8782,776.2092;Float;False;Gradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;29;376.9603,1125.598;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;32;-549.0461,1660.324;Float;False;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;31;568.0646,1156.459;Float;False;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;41;-533.1619,1903.755;Float;False;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;34;-91.21387,1746.83;Float;False;31;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;33;-313.0989,1661.383;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;45;-414.4387,2174.709;Float;False;Property;_RangeofOP;RangeofOP;8;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-206.1619,1978.755;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;156.9648,1678.156;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;531.5,434.5;Float;False;Property;_Color;Color;3;0;Create;True;0;0;False;0;1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;43;161.4652,1925.046;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;535.5,520.5;Float;False;Property;_Blend;Blend;2;0;Create;True;0;0;False;0;1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;2;499.5,249.5;Float;False;Global;_GrabScreen0;Grab Screen 0;0;0;Create;True;0;0;False;0;Object;-1;False;False;1;0;FLOAT2;0,0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;46;-815.6036,2223.665;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;534.2953,1706.554;Float;False;Opa;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;907.4941,398.6467;Float;False;40;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-1208.761,2424.188;Float;False;Constant;_SpeedBlac;SpeedBlac;8;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;369.8634,-217.3344;Float;False;Property;_Color0;Color 0;0;1;[HDR];Create;True;0;0;False;0;0.490566,0,0,0;0,0.01960784,0.01176471,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;50;-786.6537,2448.729;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;4;718.5,368.5;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector2Node;47;-1123.016,2098.861;Float;False;Constant;_Vector1;Vector 1;8;0;Create;True;0;0;False;0;5,5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleTimeNode;49;-960.8958,2443.821;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;8;491.8719,-14.62628;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;09b8aabb006c672408709caf4ef5a811;09b8aabb006c672408709caf4ef5a811;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1102.9,3.599999;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Invibleshader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;False;TransparentCutout;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;24;0;23;0
WireConnection;10;0;9;0
WireConnection;25;1;24;0
WireConnection;11;0;10;2
WireConnection;11;1;12;0
WireConnection;27;0;26;0
WireConnection;27;1;25;0
WireConnection;28;0;27;0
WireConnection;13;0;11;0
WireConnection;13;1;14;0
WireConnection;15;0;13;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;31;0;29;0
WireConnection;33;0;32;0
WireConnection;42;0;41;0
WireConnection;42;1;45;0
WireConnection;35;0;33;0
WireConnection;35;1;34;0
WireConnection;43;0;35;0
WireConnection;43;1;42;0
WireConnection;46;0;47;0
WireConnection;46;1;50;0
WireConnection;40;0;43;0
WireConnection;50;1;49;0
WireConnection;4;0;2;0
WireConnection;4;1;5;0
WireConnection;4;2;6;0
WireConnection;49;0;48;0
WireConnection;0;0;1;0
WireConnection;0;1;8;0
WireConnection;0;2;4;0
WireConnection;0;10;44;0
ASEEND*/
//CHKSM=C03062871BB87F1B25B78962629D231E1C44BE67