// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "EvilShadow"
{
	Properties
	{
		_Vector0("Vector 0", Vector) = (5,5,0,0)
		_Speed("Speed", Float) = 1
		_Float0("Float 0", Float) = 1
		_Teleport("Teleport", Range( -20 , 20)) = 0
		_range("range", Range( -20 , 20)) = 1.220364
		[HDR]_GlowColor("GlowColor", Color) = (2.996078,0.2980392,0.2509804,0)
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _GlowColor;
		uniform float2 _Vector0;
		uniform float _Speed;
		uniform float _Float0;
		uniform float _Teleport;
		uniform float _range;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			o.Normal = UnpackNormal( tex2D( _TextureSample0, uv_TextureSample0 ) );
			o.Albedo = float4(0,0,0,0).rgb;
			float mulTime6 = _Time.y * _Speed;
			float2 panner5 = ( mulTime6 * float2( -0.5,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord1 = i.uv_texcoord * _Vector0 + panner5;
			float simplePerlin2D2 = snoise( uv_TexCoord1 );
			float Noise10 = ( simplePerlin2D2 + _Float0 );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform21 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float YGradient16 = ( ( transform21.y + _Teleport ) / _range );
			float4 Emision37 = ( _GlowColor * ( Noise10 * YGradient16 ) );
			o.Emission = Emision37.rgb;
			float temp_output_30_0 = ( ( ( 1.0 - YGradient16 ) * Noise10 ) - ( Noise10 * 1.0 ) );
			float OpaMask26 = ( temp_output_30_0 + ( 1.0 - temp_output_30_0 ) );
			o.Alpha = OpaMask26;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
158;169;1918;1056;3493.646;-169.5021;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;12;-3711.51,-875.5753;Float;False;1501.54;477.4501;Noise;9;7;6;4;5;1;9;2;8;10;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-3661.51,-543.6439;Float;False;Property;_Speed;Speed;2;0;Create;True;0;0;False;0;1;1.09;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;22;-3744.565,-337.1592;Float;False;1581.091;592.7373;GradientY;7;13;15;21;20;14;19;16;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleTimeNode;6;-3479.961,-525.6277;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;13;-3694.566,-287.1592;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;5;-3280.396,-581.0623;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.5,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;21;-3409.293,-216.0957;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;4;-3282.055,-798.6476;Float;False;Property;_Vector0;Vector 0;1;0;Create;True;0;0;False;0;5,5;20,20;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;15;-3234.64,140.578;Float;False;Property;_Teleport;Teleport;4;0;Create;True;0;0;False;0;0;1.2;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;14;-3112.194,-206.4598;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-3069.067,-800.5751;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;20;-2732.854,0.6567278;Float;False;Property;_range;range;5;0;Create;True;0;0;False;0;1.220364;-0.1;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;33;-3874.943,370.3664;Float;False;2085.346;565.8243;OpaMask;10;17;28;23;25;29;24;30;31;32;26;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-2659.295,-513.1246;Float;False;Property;_Float0;Float 0;3;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;19;-2653.41,-194.8974;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;2;-2795.066,-825.5751;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;16;-2406.473,-187.8473;Float;False;YGradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-2429.16,-673.8218;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;17;-3813.924,420.3664;Float;False;16;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;23;-3578.061,425.9131;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;28;-3824.943,627.226;Float;False;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-2452.967,-810.7125;Float;False;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-3397.856,504.7411;Float;False;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-3164.319,429.4274;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;42;-3658.606,-1420.448;Float;False;1274.358;459.421;Emision;6;34;35;36;40;41;37;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-3516.836,722.5168;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;30;-2977.81,683.1907;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;35;-3608.606,-1266.95;Float;False;16;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;34;-3601.591,-1370.448;Float;False;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;40;-3391.083,-1168.027;Float;False;Property;_GlowColor;GlowColor;6;1;[HDR];Create;True;0;0;False;0;2.996078,0.2980392,0.2509804,0;1.093945,0.005160129,0.005160129,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-3315.655,-1305.543;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;31;-2570.282,758.9478;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;-2851.739,-1228.639;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;32;-2261.841,665.7961;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;26;-2032.597,445.7075;Float;False;OpaMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;49;211.8965,-482.5698;Float;False;Constant;_Color0;Color 0;8;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;37;-2627.249,-1182.156;Float;False;Emision;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-2.620487,-87.79897;Float;False;Constant;_Float1;Float 1;9;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;11;677.6635,-458.7296;Float;False;37;0;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;43;410.0266,-378.0433;Float;True;Property;_TextureSample0;Texture Sample 0;7;0;Create;True;0;0;False;0;09b8aabb006c672408709caf4ef5a811;09b8aabb006c672408709caf4ef5a811;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;27;720.8594,-137.1416;Float;False;26;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;962.3992,-516.1125;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;EvilShadow;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;7;0
WireConnection;5;1;6;0
WireConnection;21;0;13;0
WireConnection;14;0;21;2
WireConnection;14;1;15;0
WireConnection;1;0;4;0
WireConnection;1;1;5;0
WireConnection;19;0;14;0
WireConnection;19;1;20;0
WireConnection;2;0;1;0
WireConnection;16;0;19;0
WireConnection;8;0;2;0
WireConnection;8;1;9;0
WireConnection;23;0;17;0
WireConnection;10;0;8;0
WireConnection;24;0;23;0
WireConnection;24;1;25;0
WireConnection;29;0;28;0
WireConnection;30;0;24;0
WireConnection;30;1;29;0
WireConnection;36;0;34;0
WireConnection;36;1;35;0
WireConnection;31;0;30;0
WireConnection;41;0;40;0
WireConnection;41;1;36;0
WireConnection;32;0;30;0
WireConnection;32;1;31;0
WireConnection;26;0;32;0
WireConnection;37;0;41;0
WireConnection;0;0;49;0
WireConnection;0;1;43;0
WireConnection;0;2;11;0
WireConnection;0;9;27;0
ASEEND*/
//CHKSM=C58597F4CC04A169464C1221A654E3F43C8E1F47