// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "water"
{
	Properties
	{
		_UVScale("UVScale", Float) = 0
		_Color0("Color 0", Color) = (0,0.6886792,0.6814132,0)
		_Normal("Normal", 2D) = "bump" {}
		_Color1("Color 1", Color) = (0.06550375,0.6037736,0.512844,0)
		_Normal1Strenght("Normal1Strenght", Float) = 1
		_Fersnel("Fersnel", Float) = 1
		_NormalStrenght("NormalStrenght", Float) = 1
		_Normal1Strength("Normal1Strength", Float) = 1
		_UvTiling1("UvTiling1", Float) = 1
		_UvTiling2("UvTiling2", Float) = 1
		_AnimationSpeedUv1("AnimationSpeedUv1", Vector) = (0,0,0,0)
		_AnimationSpeedUV2("AnimationSpeedUV2", Vector) = (0,0,0,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
			INTERNAL_DATA
		};

		uniform sampler2D _Normal;
		uniform float _Normal1Strenght;
		uniform float2 _AnimationSpeedUv1;
		uniform float _UVScale;
		uniform float _UvTiling1;
		uniform float _Normal1Strength;
		uniform float2 _AnimationSpeedUV2;
		uniform float _UvTiling2;
		uniform float _NormalStrenght;
		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform float _Fersnel;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult2 = (float2(ase_worldPos.x , ase_worldPos.z));
			float2 myVarName6 = ( appendResult2 / _UVScale );
			float2 panner26 = ( _Time.x * _AnimationSpeedUv1 + ( myVarName6 * _UvTiling1 ));
			float2 Uv134 = panner26;
			float2 panner27 = ( _Time.x * _AnimationSpeedUV2 + ( myVarName6 * _UvTiling2 ));
			float2 Uv235 = panner27;
			float3 lerpResult11 = lerp( UnpackScaleNormal( tex2D( _Normal, Uv134 ), _Normal1Strenght ) , UnpackScaleNormal( tex2D( _Normal, Uv235 ), _Normal1Strength ) , _NormalStrenght);
			float3 NormalMap18 = lerpResult11;
			o.Normal = NormalMap18;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float fresnelNdotV20 = dot( NormalMap18, ase_worldViewDir );
			float fresnelNode20 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV20, _Fersnel ) );
			float4 lerpResult17 = lerp( _Color0 , _Color1 , fresnelNode20);
			float4 Color22 = lerpResult17;
			o.Albedo = Color22.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15600
7;29;1870;1014;3674.933;461.0739;1;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;1;-2490.505,-1101.337;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DynamicAppendNode;2;-2112.507,-1093.337;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-2059.506,-858.3369;Float;False;Property;_UVScale;UVScale;0;0;Create;True;0;0;False;0;0;6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;3;-1910.508,-1072.337;Float;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-2550.399,830.5687;Float;False;Property;_UvTiling2;UvTiling2;9;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-1782.509,-1027.337;Float;False;myVarName;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;24;-2981.504,667.7762;Float;False;6;myVarName;0;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-2538.983,577.5035;Float;False;Property;_UvTiling1;UvTiling1;8;0;Create;True;0;0;False;0;1;105.35;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-2304.854,683.0157;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TimeNode;25;-2973.96,759.8339;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;33;-2603.043,680.7768;Float;False;Property;_AnimationSpeedUv1;AnimationSpeedUv1;10;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-2306.099,779.5635;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;32;-2596.043,937.7768;Float;False;Property;_AnimationSpeedUV2;AnimationSpeedUV2;11;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PannerNode;26;-1993.712,719.5046;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;27;-2008.56,870.7097;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;37;-2717.532,98.39722;Float;False;35;Uv2;0;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-2714.532,1.397217;Float;False;34;Uv1;0;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;12;-2907.765,272.2404;Float;False;Property;_Normal1Strenght;Normal1Strenght;4;0;Create;True;0;0;False;0;1;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;35;-1728.847,887.9727;Float;False;Uv2;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-2898.765,356.2405;Float;False;Property;_Normal1Strength;Normal1Strength;7;0;Create;True;0;0;False;0;1;32.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;10;-2961.115,36.4246;Float;True;Property;_Normal;Normal;2;0;Create;True;0;0;False;0;dd2fd2df93418444c8e280f1d34deeb5;dd2fd2df93418444c8e280f1d34deeb5;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;34;-1729.469,744.676;Float;False;Uv1;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;7;-2485.302,3.072987;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;14;-2386.764,417.2404;Float;False;Property;_NormalStrenght;NormalStrenght;6;0;Create;True;0;0;False;0;1;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;8;-2476.407,205.406;Float;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;11;-2112.764,96.2406;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;21;-3013.052,-365.9019;Float;False;18;NormalMap;0;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-3041.933,-220.0739;Float;False;Property;_Fersnel;Fersnel;5;0;Create;True;0;0;False;0;1;0.15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;18;-1839.309,165.0001;Float;False;NormalMap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;15;-2738.573,-701.2072;Float;False;Property;_Color0;Color 0;1;0;Create;True;0;0;False;0;0,0.6886792,0.6814132,0;0.120283,0.2708268,0.4811321,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;16;-2738.572,-519.1516;Float;False;Property;_Color1;Color 1;3;0;Create;True;0;0;False;0;0.06550375,0.6037736,0.512844,0;0.123487,0.4034975,0.7075472,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;20;-2766.05,-332.9019;Float;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;17;-2374.464,-572.8517;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;19;-708.0259,45.14453;Float;False;18;NormalMap;0;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;22;-2150.623,-527.3231;Float;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;-697.7412,-37.36779;Float;False;22;Color;0;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-458.1176,-68.27711;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;water;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;1;1
WireConnection;2;1;1;3
WireConnection;3;0;2;0
WireConnection;3;1;5;0
WireConnection;6;0;3;0
WireConnection;28;0;24;0
WireConnection;28;1;29;0
WireConnection;31;0;24;0
WireConnection;31;1;30;0
WireConnection;26;0;28;0
WireConnection;26;2;33;0
WireConnection;26;1;25;1
WireConnection;27;0;31;0
WireConnection;27;2;32;0
WireConnection;27;1;25;1
WireConnection;35;0;27;0
WireConnection;34;0;26;0
WireConnection;7;0;10;0
WireConnection;7;1;36;0
WireConnection;7;5;12;0
WireConnection;8;0;10;0
WireConnection;8;1;37;0
WireConnection;8;5;13;0
WireConnection;11;0;7;0
WireConnection;11;1;8;0
WireConnection;11;2;14;0
WireConnection;18;0;11;0
WireConnection;20;0;21;0
WireConnection;20;3;38;0
WireConnection;17;0;15;0
WireConnection;17;1;16;0
WireConnection;17;2;20;0
WireConnection;22;0;17;0
WireConnection;0;0;23;0
WireConnection;0;1;19;0
ASEEND*/
//CHKSM=1D1D8D59543D7984936F506C691E1901DFE25B73