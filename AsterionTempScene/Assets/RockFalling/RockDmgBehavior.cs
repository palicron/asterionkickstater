﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockDmgBehavior : DamageMaker {

    public bool fall = false;
    public float speed;
	public float rotspeed;
	public GameObject hitparticle;
	private int rotdir;
	public GameObject rock;
	private Vector3 starpost;
	private GameObject sombra;
	private BoxCollider col;
    // Use this for initialization
    void Start () {
		starpost = this.gameObject.transform.position;
		rotdir =  Random.Range(-2, 2);
		sombra = GameObject.Find("BlobShadowProjector");
		col = this.GetComponent<BoxCollider>();
		col.enabled = false;
	}

    private void Update()
    {
		
        if(fall)
        {
			col.enabled = true;
			transform.position -= transform.up * speed *Time.deltaTime;
			Vector3 vec = rock.transform.rotation.eulerAngles;
			vec.x += rotspeed * rotdir * Time.deltaTime;
			vec.y += rotspeed * rotdir * Time.deltaTime;
			vec.z += rotspeed * rotdir * Time.deltaTime;
			rock.transform.rotation = Quaternion.Euler(vec);
		}

    }

    private void OnTriggerEnter(Collider other)
    {
	
		if (other.gameObject.tag.Equals("Player") || other.gameObject.tag.Equals("ground"))
		{
			hitparticle.SetActive(false);

			DamageTaker dt = other.gameObject.GetComponent<DamageTaker>();
			if (dt != null && other.gameObject.tag.Equals("Player"))
			{
				col.enabled = false;
				makeDmg(dt);
				
			}

			if (other.gameObject.tag.Equals("Player"))
			{
				
				hitparticle.transform.position = this.transform.position;
				hitparticle.SetActive(true);

				Invoke("rest", 0.0f);
			}
			else{
				
				hitparticle.transform.position = this.transform.position;
				hitparticle.SetActive(true);

				Invoke("rest", 0.0f);

			}

			sombra.SetActive(false);
		}

	
	

	}

	private void rest()
	{
		fall = false;
		
		this.gameObject.transform.position = starpost;
		
	}
	private void OnDisable()
	{
		
		
	}
	
	public override void makeDmg(DamageTaker dt)
    {
        Vector3 vec = dt.gameObject.transform.position - this.transform.position;
        vec.y = 0;
        dt.takeDmg(this.dmgMake, vec, this.dmgforce);
    }

}
