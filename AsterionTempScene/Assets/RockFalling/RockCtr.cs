﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockCtr : MonoBehaviour {


    public GameObject rock;
    public float delay;
	public GameObject particle;
    private bool star;
	private ParticleSystem par;
	
	// Use this for initialization
	void Start () {
		
		 star = false;
		// par = particle.GetComponent<ParticleSystem>();
		//par.Stop();
		//var main = par.main;
		//main.duration = delay-1.8f ;
		//main.startLifetime = delay -1.8f;
		//seteffectpos();

	}
	
	// Update is called once per frame
	void Update () {

        if (star)
		{
			//particle.SetActive(true);
			StartCoroutine(drop());
		}
			
	}

  //  private void OnTriggerEnter(Collider other)
  //  {
    //    if(other.gameObject.tag.Equals("Player"))
   //    {
    //        star = true;
    //    }
   // }

    private IEnumerator drop()
    {
		
		
		
		yield return new WaitForSecondsRealtime( delay);
		rock.GetComponent<RockDmgBehavior>().fall = true;
		star = false;
		particle.SetActive(false);
		StopAllCoroutines();



	}
    private void OnDisable()
    {
		
		StopAllCoroutines();
    }

	public void starlaunch()
	{
		star = true;
	}

	private void seteffectpos()
	{

		Vector3 origin = transform.position + (Vector3.up );

		Vector3 dir = -Vector3.up;
		float dis = 10f;
		RaycastHit hit;
		if (Physics.Raycast(origin, dir, out hit, dis))
		{
			
			Vector3 tagetposition = hit.point;

			particle.transform.position = tagetposition;
		}
	}
}
