﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerFollow : MonoBehaviour {

	public Transform camlook;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Quaternion q = camlook.rotation;
		q.x = 0;
		q.z = 0;
		this.transform.rotation = q;
	}
}
