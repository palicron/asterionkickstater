﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyDamaTaker : DamageTaker {

    public Vector3 hitpos;
    private int count=0;
    public TrainigDummyCtr ctr;
	public ParticleSystem paja;
  
	// Use this for initialization
	void Start () {
        hitpos = this.GetComponent<Transform>().position;
      

    }
	
	// Update is called once per frame
	void Update () {
        if (hit)
            count++;
        if (count > this.inviframes)
            hit = false;

	}

    public override bool takeDmg(float damg,Vector3 origin,float force)
    {
       
        
        if (hit)
            return false;

        Vector3 forcedir = hitpos - origin; 
        
       bool f =  ctr.takedmg(damg, forcedir, force);

        if (!f)
            this.enabled = false;
		hit = true;
          return true;
    }
}
