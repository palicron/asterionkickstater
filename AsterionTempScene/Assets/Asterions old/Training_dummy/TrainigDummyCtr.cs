﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainigDummyCtr : Character {


    private Animator anim;
    public string[] dmgAnimNames;
    public Animator hit;
    public Transform Player;
    public float timetodespwan;
    private enemyInView en;
    // Use this for initialization
    void Start () {
        anim = this.GetComponentInChildren<Animator>();
        en = this.GetComponentInChildren<enemyInView>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public override bool takedmg(float dmg, Vector3 origin, float force)
    {
     
        this.life -= dmg;
        if (life <= 0)
        {
            transform.LookAt(Player);
            anim.SetBool("death", true);
            en.removefronlist();
           
            Invoke("destroid", timetodespwan);
            return false;

        }
        else
        {
            hit.Play("hit");
            anim.CrossFade(dmgAnimNames[Random.Range(0, dmgAnimNames.Length - 1)], 0.3f);
            return true;

        }
     
    }

    public override float UpdateLifePor()
    {
        throw new System.NotImplementedException();
    }

    private void destroid()
    {
        GameObject.Destroy(this.gameObject);
    }
}
