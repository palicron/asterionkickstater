﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CamareaShake : MonoBehaviour {

	public Cinemachine.CinemachineVirtualCamera vc;
	Transform trans;
	bool reste=false;
	private float apcv;
	private float magcv;
	private CinemachineBasicMultiChannelPerlin cc;
	private float ampl;
	private float dura;
	private float amplf;
	private void Start()
	{
		vc = this.GetComponent<CinemachineVirtualCamera>();
		trans = this.GetComponent<Transform>();
		cc = vc.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
		apcv = cc.m_AmplitudeGain;
		magcv = cc.m_FrequencyGain;
	}
	private void Update()
	{
		if(reste)
			StartCoroutine(Shake());

	}

	public void sShake(float duration, float magnitude,float vibratefec)
	{
		ampl = magnitude;
		dura = duration;
		amplf = vibratefec;
		reste = true;
	}
	public IEnumerator Shake()
	{


		float elapsed = 0.0f;

		while (elapsed < dura)
		{
			cc.m_AmplitudeGain = ampl;
			
			cc.m_FrequencyGain = amplf ;
			elapsed += Time.deltaTime;
			yield return null;
		}
		cc.m_AmplitudeGain = apcv;
		cc.m_FrequencyGain = magcv;
		ampl = 0;
		dura = 0;
		reste = false;

	}
}
