﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummySpiderCtr : Character
{

    
    private Rigidbody rib;
    private Animator anim;
	// Use this for initialization
	void Start () {
        rib = this.GetComponentInParent<Rigidbody>();
        anim = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override bool takedmg(float dmg, Vector3 origin, float force)
    {
        life -= dmg;
        if(life<=0)
        {
            anim.CrossFade("Death", 0.2f);

            
        }
        else
        {
            rib.AddForce(origin.normalized * force, ForceMode.Impulse);
        }
        return true;

    }


    public void desanim()
    {
        anim.speed=0;
    }
    public override float UpdateLifePor()
    {
        throw new System.NotImplementedException();
    }
}
