﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummySpiderDmgTake : DamageTaker {


    private DummySpiderCtr ctr;
 

    // Use this for initialization
    void Start () {
        ctr = this.GetComponent<DummySpiderCtr>();
	}

    private void OnTriggerEnter(Collider other)
    {
        
    }
    public override bool takeDmg(float damg, Vector3 origin, float force)
    {
        float f;
        if (this.pose > force)
            f = 0;
        else
            f = force - pose;
        Vector3 forcedir = this.transform.position - origin;
        forcedir.y = 0;
        return ctr.takedmg(damg, forcedir, f);
    }
}
