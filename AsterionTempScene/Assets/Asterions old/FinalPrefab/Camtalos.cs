﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camtalos : MonoBehaviour {

	public GameObject cam1;
	public GameObject cam2;

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag.Equals("Player"))
		cam2.SetActive(!cam2.activeSelf);
		
	}
}
