﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AbilityColdDown : MonoBehaviour {


    public Image darkMask;
    public Text coolDownText;

    [SerializeField]
    private Ability ability;

    
    public Image aIcon;
    private float cDDuration;
    private float nextReadyTime;
    private float cDtimeLeft;
    private float cDDowntime;
    private bool cdComplete;
    public int pstamina;
    public Sprite icoon;
    public EventHook eh;
    public GameObject player;
    // Use this for initialization
    void Start () {
      
        eh = player.GetComponentInChildren<EventHook>();
        initilize(ability);
    }
	
	// Update is called once per frame

    public void callAbility()
    {
        ability.TriggerAbility();
       
    }
    public void initilize(Ability selectedAbility)
    {
 
        ability = selectedAbility;
        aIcon.sprite = ability.iconImage;
        eh.AbilityCollider = selectedAbility.getrefcolider();
       

    }
    public void setacol(GameObject selectedAbility)
    {
        eh.AbilityCollider = selectedAbility;
    }
   // private void  Abilityready()
  //  {
   //     coolDownText.enabled = false;
   //     darkMask.enabled = false;
   // }
    //private void CoolDown()
    //{
    //    cDtimeLeft -= Time.deltaTime;
    //    float roundedCd = Mathf.Round(cDtimeLeft);
    //    coolDownText.text = roundedCd.ToString();
    //    darkMask.fillAmount = cDtimeLeft / cDDuration;
    //}
    //private void buttonTriggerd()
    //{
    //    nextReadyTime = cDDuration + Time.time;
    //    cDDowntime = cDDuration;
    //    darkMask.enabled = true;
    //    ability.TriggerAbility();
    //}
}
