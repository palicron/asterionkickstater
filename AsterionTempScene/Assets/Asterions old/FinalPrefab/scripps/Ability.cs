﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public abstract class Ability : ScriptableObject {

    public string asName = "new Ability";
    public string animationName;
    public int staminaCost;
    public GameObject dmgCollider;
    public GameObject particleSystem;
    public GameObject weapon;
    public Asterion.Ctr_Player player;
    public float eBaseCoolDown = 1f;
    public Sprite iconImage;
    public GameObject refcolider;
    private Animator anim;


    public abstract void Init(Asterion.Ctr_Player ctr,GameObject weapon, Animator animator);
    public abstract void TriggerAbility();
    public abstract GameObject getrefcolider();

}
