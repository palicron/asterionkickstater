﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
namespace Asterion
{
    public class Ctr_Player : Character
	{

	  
        //todo atributos realacinados con controles 
		[HideInInspector]
		public float vertical;
		[HideInInspector]
		public float horizontal;
		[HideInInspector]
		public bool h_press;
		[HideInInspector]
		public bool v_press;
		[HideInInspector]
		public bool x_press;
		[HideInInspector]
		public bool b_press;
		[HideInInspector]
		public bool a_press;
		[HideInInspector]
		public bool Lb_press;
        [HideInInspector]
        public bool Lt_press;
        [HideInInspector]
        public bool rb_press;
        [HideInInspector]
        public bool rt_press;

        public List<string> AttacksNames;
		//todos los atributos relacionados con los stats vida stamina estado del jugador
		[Header("Stats")]
        public int stamina;
        public float moveMultipl;
        public float dodgeMul;
        public GameObject WeaponCollider;
		public GameObject chargueCollider;
		public List<GameObject> specialAbility;
		private float starmulti;
        private int starStamina;
		public float turnrate;
		public float stopframes;
		// estados del jugar si esta en el piso, puede moverse 
		[Header("States")]
        public float toGround;
        public bool canMove;
        public float moveAmount;
        public int aI;
		public bool equipWeapon=false;
        private bool onGround;
        [SerializeField]
        private bool onAction;
        public bool lockOn;
        public int rotatedir = 1;
        public bool alive;
        public bool hitPause = false;
        private Transform trans;
        private float delta;
        [SerializeField]
        public Animator anim;
     
        public LayerMask ignoreLayers;
        private Vector3 forward, right;  
        private Rigidbody rib;
        public float animspeed;
        public float cont = 0; 
        private Vector3 heading;
        private bool interact;  
        [Header("Components")]
        public GameObject interObjetc;
        public UIBarmanager lsmang;
		public CamareaShake sk;
		public GameObject block;
        private int numClick = 0;
		private targetController tc;
		public CinemachineFreeLook cam;
		public Transform camForward;

		public GameObject locktarget = null;

        public bool showCursor = true;
		// Use this for initialization
		private void Awake()
		{
			lsmang.init(life, stamina);
			Cursor.visible = showCursor;
		}
		void Start()
        {
            if(!showCursor)
			    Cursor.lockState = CursorLockMode.Locked;
            aI = 0;
            ignoreLayers = ~(1 << 30);
            trans = GetComponent<Transform>();
            anim = this.GetComponentInChildren<Animator>();
            canMove = true;
          //  forward = Camera.main.transform.forward; // Set forward to equal the camera's forward vector
           // forward.y = 0; // make sure y is 0
           // forward = Vector3.Normalize(forward); // make sure the length of vector is set to a max of 1.0
           // set the right-facing vector to be facing right relative to the camera's forward vector
            rib = this.GetComponent<Rigidbody>();
            interact = false;
            alive = true;
            starLife = life;
            starStamina = stamina;
           
			tc = this.GetComponent<targetController>();
			starmulti = moveMultipl;
			
		}

        // Update is called once per frame
        void Update()
        {
			forward = camForward.transform.forward;
			forward.y = 0;
			right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
			onAction = anim.GetBool("onAction");
            animspeed = anim.speed;
            delta = Time.deltaTime;
            
            DetectAction();
            if (onAction)
            {
                 if (!hitPause)
				{
					anim.speed = 1;
				}
                  //  anim.speed = attackCurve.Evaluate(cont += 1 * delta);
                else
                {
                    anim.speed = 0f;
                    if (cont > stopframes)
                    {
                       
                        cont = 0;
                        
                        hitPause = false;

                    }
                    else
                        cont++;
                }

            }
            if (onGround)
            {
                rib.drag = 4;
            }
            else
            {
                rib.drag = 0;
            }
            HandelMoveAnim();
            lockView();
            canMove = anim.GetBool("canMove");
            if (!lockOn)
            {
				//lsmang.settarget(null);
				cam.m_LookAt = this.gameObject.transform;
            }
			else
			{
				anim.SetFloat("Horizontal", horizontal);
				anim.SetFloat("Vertical", vertical);
			}
               
            if (!canMove)
                return;
            if (v_press || h_press)
                Move();
            else if (lockOn)
                DefineLockDirection();
            if (a_press)
                HandleDodge();


        }
		private void FixedUpdate()
		{
			onGround = OnGround();
			anim.SetFloat("StateTime", Mathf.Repeat(anim.GetCurrentAnimatorStateInfo(1).normalizedTime, 1f));
		}
		//metodo que calcula si el jugador esta en el piso devuelve true cuando esta en el piso false cuando no
		public bool OnGround()
        {
            bool r = false;

            Vector3 origin = transform.position + (Vector3.up * toGround);

            Vector3 dir = -Vector3.up;
            float dis = toGround + 0.3f;
            RaycastHit hit;
            if (Physics.Raycast(origin, dir, out hit, dis, ignoreLayers))
            {
                r = true;
                Vector3 tagetposition = hit.point;
				//Debug.Log("ray " + tagetposition.y + " at " + transform.position.y);
				if((tagetposition.y >= transform.position.y+0.09f) || (tagetposition.y <= transform.position.y - 0.09f) )
				{
					Vector3 v = Vector3.Lerp(transform.position, tagetposition, 0.4f);
					transform.position = v;
				}
				else
				{
					transform.position = tagetposition;
				}
			
            }


            return r;
        }
		//metodo que se encarga de la animacion del jugador dependiendo de su movimiento
        private void HandelMoveAnim()
        {
		
            anim.SetFloat("MoveSpeed", moveAmount, 0.2f, delta);
        }
		//metodo que se encarga de el doge del jugador cuanto esta en lockon como cuando no y la durecion
        private void HandleDodge()
        {
            if (!lockOn)
            {
                if (v_press || h_press)
                    anim.CrossFade("Evade Front", 0.1f);
                else
                {

                    anim.CrossFade("Dodge",0.1f);
                }
            }
            else
            {
                if (v_press || h_press)
                {
                    if(vertical==0 && horizontal==0)
                        anim.CrossFade("Dodge", 0.1f);
                    else
                        anim.CrossFade("wvades", 0.1f);



                }
               
            }
        }

		//metodo que se encarga del movimiento del jugador dependiendo si esta en lockon o no
        private void Move()
        {
            float fmove = moveAmount * moveMultipl;


            if (!lockOn)
            {
                //Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); // setup a direction Vector based on keyboard input. GetAxis returns a value between -1.0 and 1.0. If the A key is pressed, GetAxis(HorizontalKey) will return -1.0. If D is pressed, it will return 1.0
                Vector3 rightMovement = right * horizontal; // Our right movement is based on the right vector, movement speed, and our GetAxis command. We multiply by Time.deltaTime to make the movement smooth.
                Vector3 upMovement = forward  * vertical; // Up movement uses the forward vector, movement speed, and the vertical axis inputs.Vector3 heading = Vector3.Normalize(rightMovement + upMovement); // This creates our new direction. By combining our right and forward movements and normalizing them, we create a new vector that points in the appropriate direction with a length no greater than 1.0transform.forward = heading; // Sets forward direction of our game object to whatever direction we're moving in
				
				transform.position += Vector3.Normalize(rightMovement + upMovement) * fmove * Time.deltaTime;// move our transform's position right/left
																											 //transform.position += upMovement; // Move our transform's position up/dow

				heading = Vector3.Normalize(rightMovement + upMovement);
            
                transform.forward = Vector3.Lerp(transform.forward, heading, turnrate);
            }
            else
            {
				//Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); // setup a direction Vector based on keyboard input. GetAxis returns a value between -1.0 and 1.0. If the A key is pressed, GetAxis(HorizontalKey) will return -1.0. If D is pressed, it will return 1.0
				Vector3 rightMovement = right * horizontal; // Our right movement is based on the right vector, movement speed, and our GetAxis command. We multiply by Time.deltaTime to make the movement smooth.
				Vector3 upMovement = forward * vertical; // Up movement uses the forward vector, movement speed, and the vertical axis inputs.Vector3 heading = Vector3.Normalize(rightMovement + upMovement); // This creates our new direction. By combining our right and forward movements and normalizing them, we create a new vector that points in the appropriate direction with a length no greater than 1.0transform.forward = heading; // Sets forward direction of our game object to whatever direction we're moving in
				transform.position += Vector3.Normalize(rightMovement + upMovement) * fmove * Time.deltaTime*0.4f; // move our transform's position right/left
																											  //transform.position += upMovement; // Move our transform's position up/dow

				heading = Vector3.Normalize(rightMovement + upMovement);


            }




        }
		//metodo que cambia la direcion de los controles dependiendo de donde esta el jugador con respeto a su angulo ahcia el target
        private void DefineLockDirection()
        {
            Vector3 a = trans.eulerAngles;
            if (a.y > 0 && a.y < 180)
                rotatedir = 1;
            else if (a.y > 180 && a.y < 360)
                rotatedir = -1;

        }
		//metodo que se encarga de manejar todo lo realacionado con acciones del jugador ataques, iteraciones y el combo
        private void DetectAction()
        {

            int combopart = anim.GetInteger("Combo");
            int fase = anim.GetInteger("ComboIndex");
            string playanim = null;
            if (x_press && !onAction)
                playanim = AttacksNames[0];
           // else if (y_press && !onAction)
            //    playanim = AttacksNames[1];
            if(Lt_press)
			{
                anim.SetBool("block", true);
				block.SetActive(true);
				return;
			}
			else
			{
                anim.SetBool("block", false);
                block.SetActive(false);
			}
            if (onAction)
            {
                if (x_press)
                {
                    numClick++;
					anim.SetTrigger("attack");
					rib.drag = 0;
					
                }
                
            }

            if (interact && x_press)
            {
				anim.Play("Interact");
              
            }
            else if (playanim != null)
            {

                //  onAction = true;
                //   anim.SetBool("onAction", true);
                anim.SetInteger("Combo", 1);
                anim.CrossFade(playanim, 0.3f);
                numClick = 1;
            }

			if (b_press && (v_press || h_press))
			{
				anim.SetBool("Charge", true);
				moveMultipl = starmulti + 2;
				

			}
			else
			{
				anim.SetBool("Charge", false);
				moveMultipl = starmulti;
			//	rib.AddForce(trans.forward * 30f);
			}


        }
		//metodo que se encarga del lock on y ahcer que el jugador mire hacia la dirrecion del objetivo y mueve la cara a ese objetivo
        private void lockView()
        {
			if (Lb_press && !lockOn)
			{
				lockOn = true;
				anim.SetBool("lockOn", true);
				locktarget = tc.nextEnemy(0);
			}
			else  if(Lb_press && lockOn)
			{
				locktarget = null;
				anim.SetBool("lockOn", false);
			}

			if (rb_press && lockOn)
			{
				locktarget = tc.nextEnemy(1);

			}
			if (locktarget == null)
            {
                lockOn = false;
				anim.SetBool("lockOn", false);
				return;
            }


			//lsmang.settarget(tg);
			
			cam.m_LookAt = locktarget.transform;
			if (lockOn)
            {
                Vector3 tagetDir = locktarget.transform.position - trans.position;
                tagetDir.y = 0;
                Quaternion tr = Quaternion.LookRotation(tagetDir);
                Quaternion targetRotation = Quaternion.Slerp(trans.rotation, tr, delta * 2 * 2);
                trans.rotation = targetRotation;
            }

        }
		//metodo que define si el jugador esta en rago de un interactive objt y cambia el control de ataque a interac
        public void setInteract(bool inte, GameObject obj)
        {
            interact = inte;
            interObjetc = obj;
        }
		//metodo que recibe daño
        public override bool takedmg(float dmg, Vector3 origin, float force)
        {
            refreshLife(dmg);
			rib.AddForce(origin.normalized * force, ForceMode.Impulse);
			anim.Play("hit");
            if (life <= 0)
                alive = false;

			return true;
        }
		//metodo que refresca al vida en la ui
        public void refreshLife(float dmg)
        {

            life -= (int)dmg;

            lsmang.refreshlife(life);
        }
		//actuliza la estamina en el ui
        public void refreshStamina(float st)
        {
            if (stamina + st > starStamina)
                return;
            stamina += (int)st;
            lsmang.refreshstamina(stamina);
        }
		//metodo no usado de character
		public override float UpdateLifePor()
		{
			throw new System.NotImplementedException();
		}


		public bool loststam(int n)
		{
			stamina -= n;
			if (stamina < 0)
			{
				stamina = 0;
				block.SetActive(false);
				return false;
			}
				
			lsmang.refreshstamina(stamina);
			return true;
		}

		public void interac()
		{
			interObjetc.GetComponent<interactive>().action();
		}
        public void recall(Vector3 gg)
		{
			forward = gg;
			right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
		}
		public void equipweapon()
		{
			equipWeapon = true;
			anim.SetBool("weaponEquip", true);
		}
	}
}
