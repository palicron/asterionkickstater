﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class targetController : MonoBehaviour {


    enemyInView target; //Current Focused Enemy In List
    Image image;//Image Of Crosshair
    public Asterion.Ctr_Player ctr;
    bool lockedOn;//Keeps Track Of Lock On Status    

    //Tracks Which Enemy In List Is Current Target
    int lockedEnemy;

    //List of nearby enemies
    [SerializeField]
    public static List<GameObject> nearByEnemies = new List<GameObject>();
    public  List<GameObject> aa ;
    void Start()
    {
       
        image = gameObject.GetComponent<Image>();
        aa = nearByEnemies;
        lockedOn = false;
        lockedEnemy = 0;
    }

  

    public void insert(GameObject go)
    {
        nearByEnemies.Add(go);
       
    }
    public void remove(GameObject i)
    {
	

		
		 nearByEnemies.Remove(i);
		ctr.locktarget = nextEnemy(0);
	}

	public GameObject nextEnemy(int i)
	{
		if (nearByEnemies.Count == 0)
			return null;
		if(lockedEnemy +i > (nearByEnemies.Count-1))
		{
			lockedEnemy = 0;
			return nearByEnemies[lockedEnemy];
		}
		else if(lockedEnemy + i<0)
		{
			lockedEnemy = nearByEnemies.Count - 1;
			return nearByEnemies[lockedEnemy];
		}
		else
		{
			lockedEnemy += i;
			return nearByEnemies[lockedEnemy];
		}
	}

	public void delock()
	{

	}
}
