﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBarmanager : MonoBehaviour
{

    public Image life;
    public Image stamina;
    private float starLife;
    private float StarStamin;
    private float temstamp;
    private float temslife;
	public Image enemyLife;

	private void Start()
	{
		temslife = starLife;
		temstamp = StarStamin;
	}
	
    
    private void Update()
    {

        this.life.fillAmount = Mathf.Lerp(this.life.fillAmount, (temslife / starLife), 0.2f);
        this.stamina.fillAmount = Mathf.Lerp(this.stamina.fillAmount, (temstamp / StarStamin), 0.2f);
  
    }
    public void refreshall(float life, float stamina)
    {
        this.life.fillAmount = (life / starLife);
        this.stamina.fillAmount = (stamina / StarStamin);
    }
    public void refreshlife(float life)
    {
        temslife = life;
       // this.life.fillAmount = (life / starLife);
    }
    public void refreshstamina(float stam)
    {
        temstamp = stam;
      
       //this.stamina.fillAmount = (stam / StarStamin);
    }

    public void init(float life, float stamina)
    {
        this.starLife = life;
        this.StarStamin = stamina;
        refreshall(life, stamina);
    }


 
}
