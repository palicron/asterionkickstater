﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keepbools : StateMachineBehaviour {

    public string boolName;
    public bool status;
    public bool resetOnExit;
    public string boolName1;
    public bool status1;
    public bool resetOnExit1;
    public string comboname;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(boolName1, status1);
        animator.SetInteger(comboname, 0);
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        animator.SetBool(boolName, status);
        animator.SetBool(boolName1, status1);

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (resetOnExit)
            animator.SetBool(boolName, !status);
        if (resetOnExit1)
            animator.SetBool(boolName1, !status1);

    }
}
