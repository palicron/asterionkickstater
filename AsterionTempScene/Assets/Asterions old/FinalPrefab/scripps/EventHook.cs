﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHook : MonoBehaviour {


    private GameObject WeaponCollider;
	private GameObject chargeCollider;
    public GameObject AbilityCollider;
    public GameObject AbilityAnimationObjet;
	private Asterion.Ctr_Player ctr;
    private void Start()
    {
        WeaponCollider = this.GetComponentInParent<Asterion.Ctr_Player>().WeaponCollider;
		chargeCollider = this.GetComponentInParent<Asterion.Ctr_Player>().chargueCollider;
		ctr = this.GetComponentInParent<Asterion.Ctr_Player>();

	}
    public void OpenDmGCollider()
    {
        WeaponCollider.SetActive(true);
    }
    public void CloseDmGCollider()
    {
      
        WeaponCollider.SetActive(false);
    }
    public void OpenADmGCollider()
    {
        
        AbilityCollider.SetActive(true);
    }
    public void CloseADmGCollider()
    {
      
        AbilityCollider.SetActive(false);
  
    }

	public void interacAction()
	{
		ctr.interac();
	}
	public void OpenCDmGCollider()
	{
		chargeCollider.SetActive(true);
	}
	public void CloseCdmGCollider()
	{

		chargeCollider.SetActive(false);
	}
}
