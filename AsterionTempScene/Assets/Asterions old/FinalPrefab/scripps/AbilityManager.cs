﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asterion
{
    public class AbilityManager : MonoBehaviour
    {
        public float directionpress;
        public bool y_press;
        public List<Ability> Alist;
        public AbilityColdDown AbUi;
        public GameObject weapon;
       
        public float cdChange;
        private int selector;
        private float cdchange =0;
        private bool onAction;
        private float cont;
        void Start()
        {
            selector = 0;
            cont = 0;
            onAction = false;
            for (int i =0;i<Alist.Count;i++)
            {
                Alist[i].Init(this.GetComponent<Asterion.Ctr_Player>(), weapon, this.GetComponentInChildren<Animator>());
            }
            AbUi.initilize(Alist[selector]);    
        }

        // Update is called once per frame
        void Update()
        {
           if(onAction)
            {
                cont +=Time.deltaTime;
                if(cont>1.5f)
                {
                    onAction = false;
                    cont = 0;
                }
            }
            if (directionpress != 0)
            {
                
            
                AbilitySelection(directionpress);

            }
               
            if (y_press && !onAction)
            {
                onAction = true;
                AbUi.callAbility();
            }
                
        }

        public void AbilitySelection(float forward)
        {
            float oo = Time.time - cdchange;
           
            if (oo < cdChange)
                return;
            if (forward > 0)
            {
                selector++;
                if (selector > (Alist.Count - 1))
                    selector = 0;
                cdchange = Time.time;
               
                  AbUi.initilize(Alist[selector]);
            }
            if((forward < 0))
            {
                selector--;
                if (selector < 0)
                    selector = Alist.Count - 1;
                cdchange = Time.time;
                 AbUi.initilize(Alist[selector]);
            }
          
        }

    }

}
