﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asterion
{
    public class InputHandel : MonoBehaviour
    {

        [SerializeField]
        private float horizontal;
        [SerializeField]
        private float vertical;
        private bool h_press;
        private bool v_press;
        private float lr_press;
        private float ud_press;
        [SerializeField]
        private bool b_input;
        [SerializeField]
        private bool a_input;
        [SerializeField]
        private bool x_input;
        [SerializeField]
        private bool y_input;
        [SerializeField]
        private bool rb_input;
        [SerializeField]
        private float rt_axis;
        [SerializeField]
        private bool rt_input;
        [SerializeField]
        private bool lb_input;
        [SerializeField]
        private float lt_axis;
        [SerializeField]
        private bool lt_input;
        [SerializeField]
        private float scrollaxis;
        [SerializeField]
        private bool rightAxis_down;
        [SerializeField]
        private Ctr_Player control;
        private AbilityManager abilityCtr;


        private void Start()
        {
            control = this.GetComponent<Ctr_Player>();
            abilityCtr = this.GetComponent<AbilityManager>();
        }
        private void Update()
        {
            GetInput();
            UpdateStateFix();
        }

        private void GetInput()
        {
            vertical = Input.GetAxis("Vertical");
            if (Input.GetAxisRaw("Vertical") != 0)
                v_press = true;
            else
                v_press = false;
            horizontal = Input.GetAxis("Horizontal");
            if (Input.GetAxisRaw("Horizontal") != 0)
                h_press = true;
            else
                h_press = false;

           /// ud_press = Input.GetAxis("upDown");
           // lr_press = Input.GetAxis("leftRight");

           // if (Input.GetAxisRaw("Horizontal") != 0)

            rt_input = Input.GetButton("RT");
            rt_axis = Input.GetAxis("RT");

            if (rt_axis != 0)
                rt_input = true;

            lt_input = Input.GetButton("LT");
           // lt_axis = Input.GetAxis("LT");
          //  if (lt_axis != 0)
           //     lt_input = true;

            scrollaxis = Input.GetAxisRaw("scroll");

            rightAxis_down = Input.GetButtonUp("L");

            b_input = Input.GetButton("B");
            a_input = Input.GetButtonDown("A");
            x_input = Input.GetButtonDown("X");
            y_input = Input.GetButton("Y");
            rb_input = Input.GetButton("RB");
            lb_input = Input.GetButtonUp("LB");

        }
        private void UpdateStateFix()
        {
            control.vertical = vertical;
            control.horizontal = horizontal;
            control.v_press = v_press;
            control.h_press = h_press;

            control.x_press = x_input;
            control.b_press = b_input;
            control.a_press = a_input;
			control.Lb_press = lb_input;
            control.Lt_press = lt_input;
            control.rb_press = rb_input;


            abilityCtr.y_press = y_input;
            abilityCtr.directionpress = scrollaxis;

          
            float m = Mathf.Abs(vertical) + Mathf.Abs(horizontal);
            control.moveAmount = Mathf.Clamp01(m);

        }

    }
}
