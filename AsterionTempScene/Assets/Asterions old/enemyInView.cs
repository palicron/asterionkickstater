﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyInView : MonoBehaviour {
      
    bool addOnlyOnce;//This Boolean Is Used To Only Allow The Enemy To Be Added To The List Once
	public GameObject player; 
    private Transform pplay;
	private targetController tc;
    public float lockrange;
    void Start()
    {
		pplay = player.GetComponent<Transform>();
		tc = player.GetComponent<targetController>();
       
        addOnlyOnce = true;
    }

	void Update()
	{
		
			float dis = Vector3.Distance(transform.position, pplay.position);
			bool inrange = dis < lockrange;


		

        if (inrange && addOnlyOnce)
        {
            addOnlyOnce = false;
            tc.insert(this.gameObject);

        }
        if (!inrange)
        {
            
            addOnlyOnce = true;
			removefronlist();

		}
	
	}


	public void removefronlist( )
	{
		tc.remove(this.gameObject);
	}
	private void OnDestroy()
	{
		tc.remove(this.gameObject);
	}


}
