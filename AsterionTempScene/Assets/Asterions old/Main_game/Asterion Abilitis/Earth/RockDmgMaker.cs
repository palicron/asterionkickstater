﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockDmgMaker : DamageMaker
{


    public override void makeDmg(DamageTaker dt)
    {
        dt.takeDmg(this.dmgMake, originForce, this.dmgforce);
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag.Equals("Player") || other.gameObject.tag.Equals("DmgTaker") ) return;
        DamageTaker dt = other.gameObject.GetComponent<DamageTaker>();
        if (dt == null)
            return;

        this.originForce = this.transform.position;
        
        makeDmg(dt);
        this.enabled = false;
    }
}
