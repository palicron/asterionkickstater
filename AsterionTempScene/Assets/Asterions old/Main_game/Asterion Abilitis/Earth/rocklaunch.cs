﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rocklaunch : MonoBehaviour {

    public float force;
    public ForceMode mode;
    [SerializeField]
    public Rigidbody rb;
    public Asterion.Ctr_Player ctr;
    public RockDmgMaker dmg;
	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody>();
        dmg = this.GetComponent<RockDmgMaker>();
      
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void launch()
    {
        dmg.enabled = true;
        if (ctr == null)
            return;
        if(ctr.lockOn)
        {
            Vector3 v = ctr.locktarget.transform.position - transform.position;
            v.y += 0.3f;
            v = v.normalized;
            rb.AddForce(v * force, mode);
        }
        else
        {
            Vector3 v = transform.forward;
            v.y += 0.3f;
            rb.AddForce(v.normalized * force, mode);
        }
     
    }

}
