﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeCtr : MonoBehaviour {

    public Transform pjtrans;
    public GameObject rock;
    private GameObject irock;
	// Use this for initialization
	void Start () {
        pjtrans = GameObject.Find("Player").transform.Find("FloorRef");
        irock = GameObject.Instantiate(rock, pjtrans.position, pjtrans.rotation);
        irock.GetComponent<rocklaunch>().ctr = GameObject.Find("Player").GetComponent<Asterion.Ctr_Player>();
        irock.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void launchr()
    {
        irock.SetActive(false);
        
        irock.transform.position = pjtrans.position + new Vector3(0,0.5f,0);
        irock.transform.forward = pjtrans.forward;
        irock.transform.rotation = pjtrans.rotation;
        irock.GetComponent<Rigidbody>().velocity = Vector3.zero;
        irock.SetActive(true);
        irock.GetComponent<rocklaunch>().launch();
    }

    private void OnEnable()
    {


        this.transform.rotation = pjtrans.rotation;
        this.transform.position = pjtrans.position ;
    }
}
