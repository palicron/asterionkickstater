﻿using System.Collections;
using System.Collections.Generic;
using Asterion;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/EarthBase")]
public class EarthAbiility : Ability {

    private Animator anim;
    private GameObject[] earthspike;
    public int numobj;
    private int cont = 0;
    public AbilityColdDown man;
    // Use this for initialization
   

    public override void Init(Ctr_Player ctr, GameObject weapon, Animator animator)
    {
        earthspike = new GameObject[numobj];
        this.player = ctr;
        this.weapon = weapon;
        anim = animator;
        man = GameObject.Find("Ui").GetComponentInChildren<AbilityColdDown>();
      
       Transform target = GameObject.Find("Player").transform.Find("FloorRef");
        for (int i=0;i<earthspike.Length;i++)
        {
            earthspike[i] = Instantiate<GameObject>(dmgCollider, anim.transform.position, Quaternion.identity);
            earthspike[i].GetComponent<SpikeCtr>().pjtrans = target;
            earthspike[i].SetActive(false);
        }
 
    }

    public override void TriggerAbility()
    {
        if ((this.player.stamina - this.staminaCost) < 0)
            return;
        else
        {
            man.setacol(getrefcolider());
            player.refreshStamina(-this.staminaCost);

            anim.CrossFade(animationName, 0.3f);
            //this.particleSystem.SetActive(true);


        }
    }

    public override GameObject getrefcolider()
    {
       if(cont< earthspike.Length-1)
        {
            cont++;
            earthspike[cont].SetActive(false);
            return earthspike[cont];
        }
        else
        {
            cont = 0;
            earthspike[cont].SetActive(false);
            return earthspike[cont];
        }
    }
}
