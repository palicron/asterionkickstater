﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windDmgmaker : DamageMaker
{

    public  Renderer ren;
    private float cos;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        //cos += Time.deltaTime;
    }

    public override void makeDmg(DamageTaker dt)
    {
        dt.takeDmg(this.dmgMake, originForce, this.dmgforce);
    }

    private void OnTriggerEnter(Collider other)
    {
        Color old = ren.material.color;
        ren.material.color = new Color(1f, 22, 1f, cos);
        DamageTaker dt = other.gameObject.GetComponent<DamageTaker>();
        if (dt == null)
            return;

        this.originForce = this.gameObject.transform.position;
        makeDmg(dt);
    }
}
