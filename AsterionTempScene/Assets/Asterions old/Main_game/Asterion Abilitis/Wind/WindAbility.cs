﻿using System.Collections;
using System.Collections.Generic;
using Asterion;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/WindBase")]
public class WindAbility : Ability {

    private Animator anim;

   
    // Use this for initialization
    void Start () {
		
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Init(Ctr_Player ctr, GameObject weapon, Animator animator)
    {
        this.player = ctr;
        this.weapon = weapon;

        anim = animator;
        Vector3 pos = anim.gameObject.transform.position;
        pos.z = pos.z - 1;
        refcolider = Instantiate<GameObject>(dmgCollider, pos, Quaternion.identity);
        refcolider.transform.SetParent(anim.gameObject.transform);
        refcolider.SetActive(false);
    }

    public override void TriggerAbility()
    {
        if ((this.player.stamina - this.staminaCost) < 0)
            return;
        else
        {
            player.refreshStamina(-this.staminaCost);

            anim.CrossFade(animationName, 0.3f);
           
         
            
        }
    }

    public override GameObject getrefcolider()
    {
        return refcolider;
    }
}

