﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorHook : MonoBehaviour {

    public Rigidbody rib;
    public Animator anim;
	public GameObject root;

    private void Start()
    {
        rib = GetComponentInParent<Rigidbody>();
        anim = this.GetComponent<Animator>();
    }

    private void OnAnimatorMove()
    {

		//	anim.applyRootMotion = false;
		if (anim.GetBool("onAction"))
		{
   //         rib.velocity = Vector3.zero;
   //        rib.drag = 0;
   //        Vector3 delta2 = anim.deltaPosition;
		
			// delta2.y = 0;
         
			//Vector3 v = (delta2 *0.6f) / Time.deltaTime;
			// rib.velocity = v;


			rib.gameObject.transform.position = anim.rootPosition;
		}
   
    }
}
