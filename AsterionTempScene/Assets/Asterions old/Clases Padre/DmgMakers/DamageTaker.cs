﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageTaker : MonoBehaviour {

    public float armor;
    public int inviframes;
    public bool hit = false;
	public float pose;
    public abstract bool takeDmg(float damg,Vector3 origin,float force);
}
