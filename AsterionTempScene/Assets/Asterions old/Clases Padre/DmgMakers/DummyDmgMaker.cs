﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyDmgMaker : DamageMaker
{

	private Animator anim;
	private void Start()
	{
		this.originForce = this.transform.position;
		anim = this.GetComponentInParent<Animator>();
		this.unblockable = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag.Equals("Block") && !unblockable)
		{
			Vector3 dir = transform.position - other.gameObject.transform.position;
			dir.Normalize();
		
			float dot = Vector3.Dot(other.gameObject.transform.forward, dir);

			if (dot < 0)
				return;
			bool ht =  other.gameObject.GetComponent<BlockTrigger>().blockstan();
			if (!ht)
				return;
			anim.Play("block");
			this.gameObject.SetActive(false);
			return;
		}

		if (!other.gameObject.tag.Equals("DmgTaker"))
			return;
		this.originForce = this.transform.position;
		DamageTaker tk = other.gameObject.GetComponent<DamageTaker>();
		makeDmg(tk);
	}


	public override void makeDmg(DamageTaker dt)
	{
		dt.takeDmg(this.dmgMake, this.originForce, this.dmgforce);
	}
}
