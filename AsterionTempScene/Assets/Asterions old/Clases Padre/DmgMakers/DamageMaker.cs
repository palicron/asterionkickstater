﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DamageMaker : MonoBehaviour {

    public float dmgMake;
    public float dmgforce;
    public ForceMode fm;
    public Vector3 originForce;
	public bool unblockable;
    public abstract void makeDmg(DamageTaker dt);
}
