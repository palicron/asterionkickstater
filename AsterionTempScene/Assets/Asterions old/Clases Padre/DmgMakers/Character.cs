﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour {

	[HideInInspector]
    public float lifePor;
	[HideInInspector]
	public float starLife;
	public float life;
	public float pose;
    public abstract float UpdateLifePor();
	public abstract bool  takedmg(float dmg, Vector3 origin, float force);
}
