﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsterionDmgTaker : DamageTaker {

	private Character ctr;
	private bool inInbu;
	private int invurest;
	private int count;
	private void Start()
	{
		count = 0;
		inInbu = false;
		ctr = this.GetComponentInParent<Character>();
	}
	private void Update()
	{
		if(inInbu)
		{
			if (count < invurest)
			{
				count++; ;
			}
			else
			{
				this.GetComponent<CapsuleCollider>().enabled = true;
				count = 0;
				inInbu = false;
			}
		}

	}
	public override bool takeDmg(float damg, Vector3 origin, float force)
	{
		if(ctr==null)
		{
			Debug.Log("no character");
			return false;
		}
		Vector3 npos = transform.position - origin;
		float tdmg = damg - this.armor;
		float tforce = force - this.pose;
		if (tdmg < 0)
			tdmg = 0;
		if (tforce < 0)
			tforce = 0;
		invulnerabiliti(this.inviframes);
		return ctr.takedmg(tdmg, npos, tforce);

		
	}

	public void invulnerabiliti(int time)
	{
		invurest = time; 
		inInbu = true;
		this.GetComponent<CapsuleCollider>().enabled = false;

	}

}
