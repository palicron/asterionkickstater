%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Asterion_UpperBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Asterion Gray rootbone
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayBag 1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayBag 2
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayLThigh
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayLThigh/Asterion
      GrayLCalf
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayLThigh/Asterion
      GrayLCalf/Asterion GrayLFoot
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayPelvisBone003
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayRThigh
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayRThigh/Asterion
      GrayRCalf
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayRThigh/Asterion
      GrayRCalf/Asterion GrayRFoot
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1/Asterion
      GraySkirt Front2
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1/Asterion
      GraySkirt Front2/Asterion GraySkirt Front3
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1 1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1 1/Asterion
      GraySkirt Front2 1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySkirt Front1 1/Asterion
      GraySkirt Front2 1/Asterion GraySkirt Front3 1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GrayArmor Back
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GrayArmor Chest
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayCollarboneBone001
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit21
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit21/Asterion
      GrayLDigit22
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit21/Asterion
      GrayLDigit22/Asterion GrayLDigit23
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit31
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit31/Asterion
      GrayLDigit32
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit31/Asterion
      GrayLDigit32/Asterion GrayLDigit33
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit41
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit41/Asterion
      GrayLDigit42
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit41/Asterion
      GrayLDigit42/Asterion GrayLDigit43
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit51
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit51/Asterion
      GrayLDigit52
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLDigit51/Asterion
      GrayLDigit52/Asterion GrayLDigit53
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLThumb1
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLThumb1/Asterion
      GrayLThumb2
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayLCollarbone/Asterion
      GrayLUpperarm/Asterion GrayLForearm/Asterion GrayLPalm/Asterion GrayLThumb1/Asterion
      GrayLThumb2/Asterion GrayLThumb3
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion GrayLEar
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion GrayREar
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle001
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle001/Donut001
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle006
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle006/Donut002
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle011
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle011/Donut003
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle016
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayNeck1/Asterion
      GrayNeck2/Asterion GrayHead/Asterion_Gray_Base/Rectangle016/Donut004
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayCollarboneBone001 1
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit21
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit21/Asterion
      GrayRDigit22
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit21/Asterion
      GrayRDigit22/Asterion GrayRDigit23
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit31
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit31/Asterion
      GrayRDigit32
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit31/Asterion
      GrayRDigit32/Asterion GrayRDigit33
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit41
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit41/Asterion
      GrayRDigit42
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit41/Asterion
      GrayRDigit42/Asterion GrayRDigit43
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit51
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit51/Asterion
      GrayRDigit52
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRDigit51/Asterion
      GrayRDigit52/Asterion GrayRDigit53
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRThumb1
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRThumb1/Asterion
      GrayRThumb2
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Asterion GrayRThumb1/Asterion
      GrayRThumb2/Asterion GrayRThumb3
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Axe Bone
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Axe Bone/Rectangle019
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Axe Bone/Rectangle019/Donut005
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Axe Bone/Rectangle022
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GraySpine1/Asterion
      GraySpine2/Asterion GraySpine3/Asterion GrayRibcage/Asterion GrayRCollarbone/Asterion
      GrayRUpperarm/Asterion GrayRForearm/Asterion GrayRPalm/Axe Bone/Rectangle022/Donut006
    m_Weight: 1
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayTail1
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayTail1/Asterion
      GrayTail2
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayTail1/Asterion
      GrayTail2/Asterion GrayTail3
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayTail1/Asterion
      GrayTail2/Asterion GrayTail3/Asterion GrayTail4
    m_Weight: 0
  - m_Path: Asterion Gray rootbone/Asterion GrayPelvis/Asterion GrayTail1/Asterion
      GrayTail2/Asterion GrayTail3/Asterion GrayTail4/Asterion GrayTail5
    m_Weight: 0
  - m_Path: Asterion_Gray_Armadura
    m_Weight: 1
  - m_Path: Asterion_Gray_Cuerpo
    m_Weight: 0
  - m_Path: Asterion_Gray_Hacha v2
    m_Weight: 0
  - m_Path: Asterion_Gray_Head
    m_Weight: 0
  - m_Path: Slash_1
    m_Weight: 0
  - m_Path: Slash_2
    m_Weight: 0
