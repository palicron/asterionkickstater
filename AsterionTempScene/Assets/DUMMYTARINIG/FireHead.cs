﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireHead : MonoBehaviour {

    public float timebetween;
    public float flametime;
    public float durationtoal;
    public ParticleSystem[] effect;
    public GameObject fuego;
    public GameObject musicefecT;
    public GameObject light;
    private bool s1;
    private bool s2;
    private float timer;
    private float timer2;
    private float timer3;

    // Use this for initialization
    void Start() {
        s1 = false;
        s2 = false;
        timer = 0;
    }

    // Update is called once per frame
    void Update() {


        if (timer >= timebetween)
        {
            fuego.SetActive(false);
            timer = 0;
            s1 = true;
        }
        if(timer>=1)
        {
            if(s1==true)
            {
                fuego.SetActive(false);
            }
        }

  //      if (timer>=timebetween)
		//{
		//	if(!s1)
		//	{
		//		effect[0].Play();
		//		s1 = true;
		//	}
			
		//	if(timer2>=flametime && !s2)
		//	{
		//		activeEffect();
		//		s2 = true;
		//	}
		//	else
		//	{
		//		timer2 += Time.deltaTime;
		//	}

	 //          timer3 += Time.deltaTime; ;

		//}
		timer += Time.deltaTime;
		//if(timer3>=durationtoal)
		//{
		//	timer = 0;
		//	timer2 = 0;
		//	timer3 = 0;
		//	s1 = false;


		//	s2 = false;
		//	musicefecT.SetActive(false);
		//	light.SetActive(false);
		//}
	}


	private void activeEffect()
	{
		musicefecT.SetActive(true);
		light.SetActive(true);
		for (int i=1;i<effect.Length;i++)
		{
			effect[i].Play();
		}
	}
}
